api = 2
core = 7.15
projects[media][version] = 1.2
projects[media][subdir] = contrib
projects[pathologic][version] = 2.3
projects[pathologic][subdir] = contrib
projects[wordpress_migrate][version] = 2.0
projects[wordpress_migrate][subdir] = contrib
projects[wysiwyg][version] = 2.1
projects[wysiwyg][subdir] = contrib

; Libraries

libraries[tinymce][download][type] = "get"
libraries[tinymce][download][url] = "https://github.com/downloads/tinymce/tinymce/tinymce_3.4.8.zip"
libraries[tinymce][directory_name] = "tinymce"